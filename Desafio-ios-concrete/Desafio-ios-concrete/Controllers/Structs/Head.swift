//
//  Head.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 29/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import Foundation

struct Head:Decodable {
    let label :String?
    let ref   :String?
    let sha   :String?
    let user  :User?
    let repo  :Repo?
}
