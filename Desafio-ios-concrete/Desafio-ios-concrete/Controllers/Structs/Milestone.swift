//
//  Milestone.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 29/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import Foundation

struct Milestone:Decodable {
    let url:String?
    let html_url:String?
    let labels_url:String?
    let id: Double?
    let number:Double?
    let title:String?
    let description:String?
    let creator:Creator?
    let open_issues:Double?
    let closed_issues:Double?
    let state:String?
    let created_at:String?
    let updated_at:String?
    let closed_at:String?
    let due_on:String?
}
