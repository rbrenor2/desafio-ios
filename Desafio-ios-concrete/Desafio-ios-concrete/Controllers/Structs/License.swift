//
//  License.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 29/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import Foundation

struct License:Decodable {
    let key     : String?
    let name    : String?
    let spdx_id : String?
    let url     : String?
}
