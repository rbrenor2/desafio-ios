//
//  Pull.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 29/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import Foundation

struct Pull:Decodable {
    let id: Double?
    let url:String?
    let html_url:String?
    let diff_url:String?
    let patch_url:String?
    let issue_url:String?
    let number:Double?
    let state:String?
    let locked:Bool?
    let title:String?
    let user:User?
    let body:String?
    let created_at:String?
    let updated_at:String?
    let closed_at:String?
    let merged_at:String?
    let merge_commit_sha: String?
    let assignee: Assignee?
    let assignees: [Assignee]?
    let requested_reviewers: [Reviewers]?
    let milestone:Milestone?
    let commits_url:String?
    let review_comments_url:String?
    let review_comment_url:String?
    let comments_url:String?
    let statuses_url:String?
    let head:Head?
    let base:Base?
    let _links:_Links?
    let author_association:String?
}
