//
//  Reviewers.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 29/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import Foundation

struct Reviewers:Decodable {
    let login: String?
    let id: Double?
    let avatar_url: String?
    let gravatar_id: String?
    let url: String?
    let html_url: String?
    let followers_url: String?
    let following_url: String?
    let gists_url: String?
    let starred_url: String?
    let subscriptions_url: String?
    let organizations_url: String?
    let repos_url: String?
    let events_url: String?
    let received_events_url: String?
    let type: String?
    let site_admin: Bool?
}
