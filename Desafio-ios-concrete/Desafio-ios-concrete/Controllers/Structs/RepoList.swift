//
//  RepoList.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 29/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import Foundation

struct RepoList:Decodable{
    let total_count        : Int?
    let incomplete_results : Bool?
    let items              : [Repo]?
}
