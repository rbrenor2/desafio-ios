//
//  _Links.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 29/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import Foundation

struct _Links:Decodable {
    let `self`           :Link?
    let html             :Link?
    let issue            :Link?
    let comments         :Link?
    let review_comments  :Link?
    let review_comment   :Link?
    let commits          :Link?
    let statuses         :Link?
}
