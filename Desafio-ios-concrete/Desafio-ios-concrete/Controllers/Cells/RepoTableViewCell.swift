//
//  RepoTableViewCell.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 26/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell {

    @IBOutlet weak var repoCellTitle: UILabel!
    @IBOutlet weak var repoCellDescription: UILabel!
    @IBOutlet weak var repoCellForkIcon: UIImageView!
    @IBOutlet weak var repoCellForkNumber: UILabel!
    @IBOutlet weak var repoCellStarIcon: UIImageView!
    @IBOutlet weak var repoCellStarNumber: UILabel!
    @IBOutlet weak var repoCellUserAvatar: UIImageView!
    @IBOutlet weak var repoCellUsername: UILabel!
    @IBOutlet weak var repoCellName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
