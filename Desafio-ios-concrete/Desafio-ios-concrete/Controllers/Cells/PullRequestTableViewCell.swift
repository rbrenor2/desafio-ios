//
//  PullRequestTableViewCell.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 28/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import UIKit


class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var pullCreatedAt: UILabel!
    @IBOutlet weak var pullName: UILabel!
    @IBOutlet weak var pullUsername: UILabel!
    @IBOutlet weak var pullAvatarUser: UIImageView!
    @IBOutlet weak var pullDescription: UILabel!
    @IBOutlet weak var pullTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
