//
//  ViewController.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 26/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage



class RepoListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var repoTableView: UITableView!
    var repoListArray:Array<Repo> = []
    var selectedRepo: Any         = 0
    var currentPage               = 1
    var numberOfPages             = 0
    
    //
    let activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Set view controller title
        self.navigationItem.title = "Github JavaPop"
        
        //
        repoTableView.delegate   = self
        repoTableView.dataSource = self
        
        //
        self.activityIndicator.frame = CGRect(x: (self.view.bounds.width/2) - 25, y: (self.view.bounds.height/2) - 25, width: 50, height: 50)
        self.activityIndicator.activityIndicatorViewStyle = .gray
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.view.addSubview(self.activityIndicator)

       //
       getData()
    }
    
    func getData() {
        
        let itemsPerPage       = 10
        let itemsPerPageString = "per_page=" + String(itemsPerPage)
        let pageString         = "&page=" + String(self.currentPage)
        let jsonUrlPathBase    = "https://api.github.com/search/repositories?q=language:Java&sort=stars&"
        let jsonPath           = jsonUrlPathBase + itemsPerPageString + pageString
        print("A. Path being downloaded: " + jsonPath)
        
        //Get background queue and fetch the data
        DispatchQueue.global(qos: .background).async {
            Alamofire.request(jsonPath).responseJSON(completionHandler: { response in
                    print("1. Getting data.")
                if let data = response.data {
                    print("2. Data is not nil.")
                    do {
                        let decodedRepos = try JSONDecoder().decode(RepoList.self, from: data)
                        print("3. Decoded data with success.")
                        
                        if decodedRepos.items != nil {
                            if self.currentPage == 1 {
                                let totalCount     = decodedRepos.total_count!
                                self.numberOfPages = (totalCount/itemsPerPage)
                                print("OBS: Number of pages to download: " + String(self.numberOfPages))
                            }
                            
                            print("4. Repos are not nil.")
                            self.repoListArray.append(contentsOf: decodedRepos.items!)
                            self.currentPage += 1
                        }
                        DispatchQueue.main.async {
                            print("5. Realoading data with success")
                            //reload tableview
                            self.repoTableView.reloadData()
                            
                            //stop activity indicator
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                        }
                        
                    } catch let error {
                        print("5e. Error: " + error.localizedDescription)
                    }
                }
            })
        }
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repoListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseID = "repoCell"
        let cell    = repoTableView.dequeueReusableCell(withIdentifier: reuseID, for: indexPath) as! RepoTableViewCell

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRepo = repoListArray[indexPath.row]
        self.performSegue(withIdentifier: "getPullsList", sender: self)
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let newRepo = repoListArray[indexPath.row]
        let oldCell = cell as! RepoTableViewCell
        
        oldCell.repoCellTitle.text       = newRepo.name
        oldCell.repoCellName.text        = newRepo.full_name
        oldCell.repoCellUsername.text    = newRepo.owner?.login
        oldCell.repoCellForkNumber.text  = String(describing:newRepo.forks_count!)
        oldCell.repoCellStarNumber.text  = String(describing:newRepo.stargazers_count!)
        oldCell.repoCellDescription.text = newRepo.description
        
        //Test if the image is cached, if it is, fetch from cache, if not fetch from url and add to cache.
        let imageCache = AutoPurgingImageCache()
        DispatchQueue.global(qos: .background).async {
            if let fetchedImage = imageCache.image(withIdentifier: (self.repoListArray[indexPath.row].owner?.login)!) {
                DispatchQueue.main.async {
                    oldCell.repoCellUserAvatar.image = fetchedImage
                }
            } else {
                let userLogin   = newRepo.owner?.login
                let apiBaseCall = "https://github.com/"
                let avatarPath  = apiBaseCall + userLogin! + ".png"
                print(avatarPath)
                
                Alamofire.request(avatarPath).responseImage { response in
                    if let image = response.result.value{
                        //Add avatar image to cache
                        let roundAvatarImage = image.af_imageRoundedIntoCircle()
                        imageCache.add(roundAvatarImage, withIdentifier: userLogin!)
                        //Add to UI
                        DispatchQueue.main.async {
                            oldCell.repoCellUserAvatar.image = roundAvatarImage
                        }
                    }
                }
            }
        }
    }

    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height

        if (bottomEdge >= scrollView.contentSize.height) {
            // Content bottom reached
            getData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let pullsListViewController : PullRequestListViewController = segue.destination as! PullRequestListViewController
        let selectedRepo = self.selectedRepo as! Repo
        
        pullsListViewController.repoTitle    = selectedRepo.name!
        pullsListViewController.repoUsername = (selectedRepo.owner?.login)!
        pullsListViewController.repoName     = selectedRepo.full_name!
     
        
        
    }
}

