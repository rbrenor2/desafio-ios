//
//  PullRequestListViewController.swift
//  Desafio-ios-concrete
//
//  Created by Breno Ramos on 28/12/17.
//  Copyright © 2017 brenor2. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PullRequestListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var pullsTableView: UITableView!
    var repoName     : String = ""
    var repoUsername : String = ""
    var repoTitle    : String = ""
    
    var currentPage = 1
    var numberOfPages : Int = 0
    var pullsArray : Array<Pull> = []
    
    let activityIndicator = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        pullsTableView.delegate = self
        pullsTableView.dataSource = self
        
        //
        self.navigationItem.title = repoName + " Pulls"
        
        //
        self.activityIndicator.frame = CGRect(x: (self.view.bounds.width/2) - 25, y: (self.view.bounds.height/2) - 25, width: 50, height: 50)
        self.activityIndicator.activityIndicatorViewStyle = .gray
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.view.addSubview(self.activityIndicator)

        //
        getData()
    }
    
    func getData() {
    
        let jsonUrlPathBase    = "https://api.github.com/repos/"
        let jsonPath           = jsonUrlPathBase + self.repoUsername + "/" + self.repoTitle + "/pulls"
        print("B. Path being downloaded: " + jsonPath)
        
        //get background queue and fetch the data
        DispatchQueue.global(qos: .background).async {
            Alamofire.request(jsonPath).responseJSON(completionHandler: { response in
                print("1. Getting data.")
                if let data = response.data {
                    print()
                    print("2. Data is not nil.")
                    do {
                        //decode
                        let decodedPulls = try JSONDecoder().decode([Pull].self, from: data)
                        print("3. Decoded data with success.")
                        print(decodedPulls)
                        
                        //append to the tableview array
                        print("4. Repos are not nil.")
                        self.pullsArray.append(contentsOf: decodedPulls)
                        self.currentPage += 1
                    
                        DispatchQueue.main.async {
                            //reload tableview data
                            print("5. Realoading data with success")
                            self.pullsTableView.reloadData()
                            
                            //stop activity indicator
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                        }
                        
                    } catch let error {
                        print(error)
                    }
                }
            })
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = pullsTableView.dequeueReusableCell(withIdentifier: "pullCell") as! PullRequestTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let newPull = pullsArray[indexPath.row]
        let oldCell = cell as! PullRequestTableViewCell
        
        print(newPull)
        
        oldCell.pullTitle.text        = newPull.title
        oldCell.pullName.text         = self.repoName
        oldCell.pullUsername.text     = newPull.user?.login
        oldCell.pullDescription.text  = newPull.body
        
        oldCell.pullCreatedAt.text = formatDate(dateString: newPull.created_at!)
        
        //Test if the image is cached, if it is, fetch from cache, if not fetch from url and add to cache.
        let imageCache = AutoPurgingImageCache()
        DispatchQueue.global(qos: .background).async {
            if let fetchedImage = imageCache.image(withIdentifier: (self.pullsArray[indexPath.row].user?.login)!) {
                DispatchQueue.main.async {
                    oldCell.pullAvatarUser.image = fetchedImage
                }
            } else {
                let userLogin   = newPull.user?.login
                let apiBaseCall = "https://github.com/"
                let avatarPath  = apiBaseCall + userLogin! + ".png"
                print(avatarPath)
                
                Alamofire.request(avatarPath).responseImage { response in
                    if let image = response.result.value{
                        //Add avatar image to cache
                        let roundAvatarImage = image.af_imageRoundedIntoCircle()
                        imageCache.add(roundAvatarImage, withIdentifier: userLogin!)
                        //Add to UI
                        DispatchQueue.main.async {
                            oldCell.pullAvatarUser.image = roundAvatarImage
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // open html_url of the pull selected
        let url = URL(string : pullsArray[indexPath.row].html_url!)
        UIApplication.shared.openURL(url!)
    }
    
    func formatDate(dateString:String) -> String{
        //set a date formatter
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        //get  day/month/year components
        let calendar        = Calendar.current
        let createdAtDate   = dateFormatter.date(from: dateString)
        let components = calendar.dateComponents([.day, .month, .year], from: createdAtDate!)
        let day             = components.day
        let month           = components.month
        let year            = components.year
        
        //
        let date = String(describing: day!) + "/" + String(describing:month!) + "/" + String(describing:year!)
        
        return date
    }
}



